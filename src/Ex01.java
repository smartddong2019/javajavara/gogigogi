
public class Ex01 {

	public static void main(String[] args) {
		// syso 입력 후 ctrl+스페이스바는 System.out.println(); 자동생성하는 단축키!!!

		// ctrl+슬래쉬는 한줄 주석!
		/*
		 *  
		 * ctrl+shift+/는 범위 주석!
		 */

		int a = 5;
		System.out.println(a);
		a = 10;
		System.out.println(a);

	}

}
