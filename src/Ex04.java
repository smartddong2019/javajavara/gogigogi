import java.util.Scanner;

public class Ex04 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		//소수점 두자리까지 볼수있는거 한줄로 정리쓰!!! ㅜ
		//실수형 상자에 넣기 이전에 정수를 실수로 나누어야 소수점이 보임!!! => 실수형으로 강제로 바꾸던가 3.0처럼 실수로 나누면 됨..!!
				
		System.out.print("your java score : ");
		int java = sc.nextInt();
		System.out.print("your web score : ");
		int web = sc.nextInt();
		System.out.print("your android score : ");
		int and = sc.nextInt();

		sc.close();
		int total = java + web + and;
		double avg = ((int)total/3.0*100)/100.0;
		
		System.out.println("합계는 : " + total);
		System.out.println("평균은 : " + avg);
		
		
		
	}

}
