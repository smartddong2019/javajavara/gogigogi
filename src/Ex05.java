import java.util.Scanner;

public class Ex05 {

	public static void main(String[] args) {
		// 돈을 받아서 잔돈을 계산해주는 프로그램 작성
		// ex) 28700
		Scanner sc = new Scanner(System.in);
	
		System.out.print("Show me the money!!! : ");
		int money = sc.nextInt();
		int jan;
		
		jan = money / 50000;
		System.out.println("50000원 : " + jan + "개" );
		money = money % 50000;
		
		jan = money / 10000;
		System.out.println("10000원 : " + jan + "개" );
		money = money % 10000;
		
		jan = money / 5000;
		System.out.println("5000원 : " + jan + "개" );
		money = money % 5000;

		jan = money / 1000;
		System.out.println("1000원 : " + jan + "개" );
		money = money % 1000;
		
		jan = money / 500;
		System.out.println("500원 : " + jan + "개" );
		money = money % 500;
		
		jan = money / 100;
		System.out.println("100원 : " + jan + "개" );
		money = money % 100;
		

		sc.close();
	}

}
